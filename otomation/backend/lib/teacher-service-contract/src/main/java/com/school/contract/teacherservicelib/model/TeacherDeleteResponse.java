package com.school.contract.teacherservicelib.model;


import com.school.contract.base.model.BaseResponse;
import lombok.*;

import java.io.Serializable;

/**
 * @author Eyüp Akay
 */

@Getter
@Setter
@AllArgsConstructor
@Builder
public class TeacherDeleteResponse extends BaseResponse implements Serializable {

}
