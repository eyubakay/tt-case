package com.school.contract.teacherservicelib.model;

import lombok.*;

import java.io.Serializable;

/**
 * @author Eyüp Akay
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TeacherAddRequest implements Serializable {

    private TeacherDto teacher;
}
