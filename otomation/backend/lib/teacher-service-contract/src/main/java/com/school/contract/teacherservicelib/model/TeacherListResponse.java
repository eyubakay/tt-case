package com.school.contract.teacherservicelib.model;


import com.school.contract.base.model.BaseResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @author Eyüp Akay
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TeacherListResponse extends BaseResponse implements Serializable {
    private List<TeacherDto> teacherList;
}
