package com.school.contract.teacherservicelib.model;

import lombok.*;

import java.io.Serializable;

/**
 * @author Eyüp Akay
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TeacherDeleteRequest implements Serializable {
    private int id;
}
