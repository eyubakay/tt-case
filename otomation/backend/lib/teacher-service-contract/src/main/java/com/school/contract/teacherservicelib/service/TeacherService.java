package com.school.contract.teacherservicelib.service;

import com.school.contract.teacherservicelib.model.*;

/**
 * @author Eyüp Akay
 */
public interface TeacherService {

    TeacherAddResponse add(TeacherAddRequest request);

    TeacherUpdateResponse update(TeacherUpdateRequest request);

    TeacherGetResponse get(TeacherGetRequest request);

    TeacherListResponse list(TeacherListRequest request);

    TeacherDeleteResponse delete(TeacherDeleteRequest request);
}
