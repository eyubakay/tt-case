package com.school.contract.teacherservicelib.model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TeacherDto implements Serializable {

    private Integer id;
    private String name;
    private String surname;
    private String mail;
    private String phone;
    private String branch;
}
