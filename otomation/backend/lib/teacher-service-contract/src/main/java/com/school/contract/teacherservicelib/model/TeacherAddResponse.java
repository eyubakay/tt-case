package com.school.contract.teacherservicelib.model;


import com.school.contract.base.model.BaseResponse;
import lombok.*;

import java.io.Serializable;

/**
 * @author Eyüp Akay
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TeacherAddResponse extends BaseResponse implements Serializable {
    private TeacherDto teacher;
}
