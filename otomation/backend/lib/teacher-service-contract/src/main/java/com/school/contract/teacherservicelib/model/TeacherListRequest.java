package com.school.contract.teacherservicelib.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author Eyüp Akay
 */
@Getter
@Setter
@AllArgsConstructor
public class TeacherListRequest implements Serializable {

}
