package com.school.contract.studentservicelib.model;

import lombok.*;

import java.io.Serializable;

/**
 * @author Eyüp Akay
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StudentDeleteRequest implements Serializable {
    private Integer id;
}
