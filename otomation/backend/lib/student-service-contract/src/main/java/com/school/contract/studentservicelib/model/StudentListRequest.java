package com.school.contract.studentservicelib.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author Eyüp Akay
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
public class StudentListRequest implements Serializable {

}
