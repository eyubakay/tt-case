package com.school.contract.studentservicelib.model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StudentDto implements Serializable {
    private Integer id;
    private String name;
    private String surname;
}
