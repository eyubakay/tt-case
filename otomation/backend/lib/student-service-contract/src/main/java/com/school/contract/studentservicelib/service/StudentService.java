package com.school.contract.studentservicelib.service;

import com.school.contract.studentservicelib.model.*;

/**
 * @author Eyüp Akay
 */
public interface StudentService {

    StudentAddResponse add(StudentAddRequest request);

    StudentUpdateResponse update(StudentUpdateRequest request);

    StudentGetResponse get(StudentGetRequest request);

    StudentListResponse list(StudentListRequest request);

    StudentDeleteResponse delete(StudentDeleteRequest request);
}
