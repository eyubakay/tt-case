package com.school.contract.studentservicelib.model;


import com.school.contract.base.model.BaseResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author Eyüp Akay
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
public class StudentDeleteResponse extends BaseResponse implements Serializable {

}
