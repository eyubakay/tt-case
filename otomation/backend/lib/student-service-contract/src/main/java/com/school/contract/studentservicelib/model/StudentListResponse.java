package com.school.contract.studentservicelib.model;


import com.school.contract.base.model.BaseResponse;
import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * @author Eyüp Akay
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StudentListResponse extends BaseResponse implements Serializable {
    private List<StudentDto> studentList;
}
