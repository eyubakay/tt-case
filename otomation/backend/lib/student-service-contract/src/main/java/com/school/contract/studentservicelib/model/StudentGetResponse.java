package com.school.contract.studentservicelib.model;


import com.school.contract.base.model.BaseResponse;
import lombok.*;

import java.io.Serializable;

/**
 * @author Eyüp Akay
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StudentGetResponse extends BaseResponse implements Serializable {
    private StudentDto student;
}
