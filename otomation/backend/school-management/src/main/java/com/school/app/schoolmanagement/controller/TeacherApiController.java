package com.school.app.schoolmanagement.controller;

import com.google.gson.Gson;
import com.school.app.schoolmanagement.api.TeacherApi;
import com.school.common.lib.aop.annotation.Loggable;
import com.school.contract.teacherservicelib.model.*;
import com.school.contract.teacherservicelib.service.TeacherService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * @author Eyüp Akay
 */
@RestController
@Loggable
public class TeacherApiController implements TeacherApi {

    private Logger logger = Logger.getLogger(this.getClass().getName());
    @Autowired
    private TeacherService teacherService;
    @Autowired
    private Gson gson;


    @Override
    public ResponseEntity<TeacherAddResponse> add(TeacherAddRequest request) {
        return new ResponseEntity<>(teacherService.add(request), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<TeacherGetResponse> get(String id) {
        if (Objects.isNull(id) || id.length() == 0) {
            throw new RuntimeException("Invalid Id");
        }
        return new ResponseEntity<>(teacherService.get(TeacherGetRequest.builder().teacher(TeacherDto.builder().id(Integer.valueOf(id)).build()).build()), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<TeacherDeleteResponse> delete(TeacherDeleteRequest request) {
        return new ResponseEntity<>(teacherService.delete(request), HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<TeacherUpdateResponse> update(TeacherUpdateRequest request) {
        return new ResponseEntity<>(teacherService.update(request), HttpStatus.ACCEPTED);
    }

    @Override
    public ResponseEntity<TeacherListResponse> list(TeacherListRequest request) {
        return new ResponseEntity<>(teacherService.list(request), HttpStatus.OK);
    }

    public void setTeacherService(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    public void setGson(Gson gson) {
        this.gson = gson;
    }
}
