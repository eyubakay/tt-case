package com.school.app.schoolmanagement.config;

import com.school.contract.studentservicelib.service.StudentService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;

@Configuration
public class StudentServiceConfig {

    @Value("${service.student.url}")
    private String url;

    @Bean
    public HttpInvokerProxyFactoryBean studentServiceInvoker() {
        HttpInvokerProxyFactoryBean invoker = new HttpInvokerProxyFactoryBean();
        invoker.setServiceUrl(url);
        invoker.setServiceInterface(StudentService.class);
        return invoker;
    }

}
