package com.school.app.schoolmanagement.controller;

import com.google.gson.Gson;
import com.school.app.schoolmanagement.api.StudentApi;
import com.school.common.lib.aop.annotation.Loggable;
import com.school.contract.studentservicelib.model.*;
import com.school.contract.studentservicelib.service.StudentService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * @author Eyüp Akay
 */
@RestController
@Loggable
public class StudentApiController implements StudentApi {

    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Autowired
    private StudentService studentService;

    @Autowired
    private Gson gson;

    public void setGson(Gson gson) {
        this.gson = gson;
    }

    @Override
    public ResponseEntity<StudentAddResponse> add(StudentAddRequest request) {
        StudentAddResponse response = studentService.add(request);
        if (response.getStatus()) {
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<StudentGetResponse> get(String id) {
        if (Objects.isNull(id) || id.length() == 0) {
            throw new RuntimeException("Invalid Id");
        }
        StudentGetResponse response = studentService.get(StudentGetRequest.builder().id(Integer.valueOf(id)).build());
        if( response.getStatus() ) {
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<StudentDeleteResponse> delete(StudentDeleteRequest request) {
        StudentDeleteResponse response = studentService.delete(request);
        if (studentService.delete(request).getStatus()) {
            return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<StudentUpdateResponse> update(StudentUpdateRequest request) {
        StudentUpdateResponse response = studentService.update(request);
        if (response.getStatus()) {
            return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<StudentListResponse> list() {
        StudentListResponse response = studentService.list(StudentListRequest.builder().build());
        if (response.getStatus()) {
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }


    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }
}
