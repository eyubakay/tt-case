package com.school.app.schoolmanagement.api;

import com.school.contract.studentservicelib.model.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Eyüp Akay
 */
@RequestMapping("/student")
public interface StudentApi {

    @PostMapping(
            produces = {"application/json;charset=utf-8"},
            consumes = {"application/json;charset=utf-8"})
    ResponseEntity<StudentAddResponse> add(@RequestBody StudentAddRequest request);

    @GetMapping(value = "/{id}",
            produces = {"application/json;charset=utf-8"},
            consumes = {"application/json;charset=utf-8"})
    ResponseEntity<StudentGetResponse> get(@PathVariable("id") String id);

    @DeleteMapping(
            produces = {"application/json;charset=utf-8"},
            consumes = {"application/json;charset=utf-8"})
    ResponseEntity<StudentDeleteResponse> delete(@RequestBody StudentDeleteRequest request);

    @PatchMapping(
            produces = {"application/json;charset=utf-8"},
            consumes = {"application/json;charset=utf-8"})
    ResponseEntity<StudentUpdateResponse> update(@RequestBody StudentUpdateRequest request);

    @GetMapping(
            produces = {"application/json;charset=utf-8"},
            consumes = {"application/json;charset=utf-8"})
    ResponseEntity<StudentListResponse> list();
}
