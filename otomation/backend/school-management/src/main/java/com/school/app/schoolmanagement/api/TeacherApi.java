package com.school.app.schoolmanagement.api;

import com.school.contract.teacherservicelib.model.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Eyüp Akay
 */
@RequestMapping("/teacher")
public interface TeacherApi {

    @PostMapping
    ResponseEntity<TeacherAddResponse> add(@RequestBody TeacherAddRequest request);

    @GetMapping("/{id}")
    ResponseEntity<TeacherGetResponse> get(@PathVariable("id") String id);

    @DeleteMapping
    ResponseEntity<TeacherDeleteResponse> delete(@RequestBody TeacherDeleteRequest request);

    @PatchMapping
    ResponseEntity<TeacherUpdateResponse> update(@RequestBody TeacherUpdateRequest request);

    @GetMapping
    ResponseEntity<TeacherListResponse> list(@RequestBody TeacherListRequest request);

}
