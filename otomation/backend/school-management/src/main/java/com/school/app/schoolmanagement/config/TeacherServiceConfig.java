package com.school.app.schoolmanagement.config;

import com.school.contract.teacherservicelib.service.TeacherService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;

@Configuration
public class TeacherServiceConfig {

    @Value("${service.teacher.url}")
    private String url;

    @Bean
    public HttpInvokerProxyFactoryBean teacherServiceInvoker() {
        HttpInvokerProxyFactoryBean invoker = new HttpInvokerProxyFactoryBean();
        invoker.setServiceUrl(url);
        invoker.setServiceInterface(TeacherService.class);
        return invoker;
    }

}
