package com.school.app.studentservice.service;

import com.school.app.studentservice.dto.StudentDtoMapper;
import com.school.app.studentservice.entity.Student;
import com.school.app.studentservice.repository.StudentRepository;
import com.school.contract.studentservicelib.model.StudentAddRequest;
import com.school.contract.studentservicelib.model.StudentGetRequest;
import com.school.contract.studentservicelib.model.StudentGetResponse;
import com.school.contract.studentservicelib.service.StudentService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class StudentServiceTests {


    @Autowired
    StudentService studentService;

    @Autowired
    StudentRepositoryService repositoryService;

    @Autowired
    StudentRepository repository;

    @BeforeEach
    void setUp() {
        repository.deleteAll();
    }

    @Test
    @Order(2)
    void test_repository_add_success() {
        Student expectedStudent = getStudent();

        repository.save(expectedStudent);

        Student actualStudent = repository.findStudentById(1);

        Assertions.assertNotNull(actualStudent);
        Assertions.assertNotNull(actualStudent.getId());
        Assertions.assertEquals(expectedStudent.getName(), actualStudent.getName());
    }

    @Test
    @Order(1)
    void test_add_success() {
        StudentAddRequest expectedStudent = StudentAddRequest.builder()
                .student(StudentDtoMapper.getStudentDtoFromStudentEntity(getStudent()))
                .build();

        studentService.add(expectedStudent);

        StudentGetResponse response = studentService.get(StudentGetRequest.builder()
                .id(1).build());

        Assertions.assertNotNull(response);
        Assertions.assertTrue(response.getStatus());
        Assertions.assertNotNull(response.getStudent().getName());
        Assertions.assertEquals(expectedStudent.getStudent().getName(), response.getStudent().getName());
    }

    private Student getStudent() {
        return Student.builder()
                .name("Eyüp")
                .surname("Akay")
                .build();
    }


}
