package com.school.app.studentservice.dto;

import com.school.app.studentservice.entity.Student;
import com.school.contract.studentservicelib.model.StudentDto;

public class StudentDtoMapper {

    public static Student getStudentEntityFromStudentDto(StudentDto student) {
        return Student.builder()
                .id(student.getId())
                .name(student.getName())
                .surname(student.getSurname())
                .build();
    }


    public static StudentDto getStudentDtoFromStudentEntity(Student student) {
        return StudentDto.builder()
                .name(student.getName())
                .surname(student.getSurname())
                .id(student.getId())
                .build();
    }


}
