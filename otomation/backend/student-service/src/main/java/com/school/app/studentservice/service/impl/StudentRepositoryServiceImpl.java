package com.school.app.studentservice.service.impl;

import com.school.app.studentservice.entity.Student;
import com.school.app.studentservice.repository.StudentRepository;
import com.school.app.studentservice.service.StudentRepositoryService;
import com.school.common.lib.aop.annotation.Loggable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
@Loggable
public class StudentRepositoryServiceImpl implements StudentRepositoryService {

    @Autowired
    private StudentRepository repository;

    @Override
    public Student add(Student student) {
        return repository.save(student);
    }

    @Override
    @Transactional
    public boolean delete(Integer id) {
        return repository.deleteStudentById(id)>0;
    }

    @Override
    public Student update(Student student) {
        return null;
    }

    @Override
    public Student get(Integer id) {
        return repository.findStudentById(id);
    }

    @Override
    public List<Student> list() {
        return repository.findAll();
    }
}
