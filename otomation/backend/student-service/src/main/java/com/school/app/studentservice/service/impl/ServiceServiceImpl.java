package com.school.app.studentservice.service.impl;

import com.google.gson.Gson;
import com.school.app.studentservice.dto.StudentDtoMapper;
import com.school.app.studentservice.entity.Student;
import com.school.app.studentservice.service.StudentRepositoryService;
import com.school.common.lib.aop.annotation.Loggable;
import com.school.contract.studentservicelib.model.*;
import com.school.contract.studentservicelib.service.StudentService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Eyüp Akay
 */
@Loggable
@Service
public class ServiceServiceImpl implements StudentService {

    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Autowired
    private Gson gson;

    @Autowired
    private StudentRepositoryService repositoryService;


    @Override
    public StudentAddResponse add(StudentAddRequest studentAddRequest) {
        Student student = repositoryService.add(
                StudentDtoMapper.getStudentEntityFromStudentDto(studentAddRequest.getStudent()));

        StudentAddResponse response = StudentAddResponse.builder().build();

        if (Objects.isNull(student) || Objects.isNull(student.getId())) {
            response.setStatus(false);
            response.setMessage("Success!");
        }

        StudentDto studentDto = StudentDtoMapper.getStudentDtoFromStudentEntity(student);

        response.setStudent(studentDto);
        response.setStatus(true);
        response.setMessage("Successfully Saved!");

        return response;
    }

    @Override
    public StudentUpdateResponse update(StudentUpdateRequest studentUpdateRequest) {
        return null;
    }

    @Override
    public StudentGetResponse get(StudentGetRequest studentGetRequest) {
        Student student = repositoryService.get(studentGetRequest.getId());

        StudentGetResponse response = StudentGetResponse.builder().build();

        if (Objects.isNull(student) || Objects.isNull(student.getId())) {
            response.setStatus(false);
            response.setMessage("Fail!");
        }

        response.setStudent(StudentDtoMapper.getStudentDtoFromStudentEntity(repositoryService.get(studentGetRequest.getId())));
        response.setStatus(true);
        response.setMessage("Success!");

        return response;
    }

    @Override
    public StudentListResponse list(StudentListRequest studentListRequest) {
        List<Student> studentList = repositoryService.list();
        StudentListResponse response = StudentListResponse.builder().build();
        if (Objects.isNull(studentList) || Objects.isNull(studentList.isEmpty()) || studentList.size() == 0) {
            response.setStatus(false);
            response.setMessage("Fail!");
        }

        List<StudentDto> students = new ArrayList<>();
        for ( Student s : studentList) {
            students.add(StudentDtoMapper.getStudentDtoFromStudentEntity(s));
        }
        response.setStudentList(students);
        response.setStatus(true);
        response.setMessage("Success!");
        return response;
    }

    @Override
    public StudentDeleteResponse delete(StudentDeleteRequest studentDeleteRequest) {
        boolean result = repositoryService.delete(studentDeleteRequest.getId());

        StudentDeleteResponse response = StudentDeleteResponse.builder().build();
        if (!result) {
            response.setStatus(false);
            response.setMessage("Fail!");
        }

        response.setStatus(true);
        response.setMessage("Success!");

        return response;
    }

    public void setGson(Gson gson) {
        this.gson = gson;
    }

    public void setRepositoryService(StudentRepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }

}
