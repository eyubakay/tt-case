package com.school.app.studentservice.repository;

import com.school.app.studentservice.entity.Student;
import com.school.common.lib.aop.annotation.Loggable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * @author Eyüp Akay
 */
@Loggable
@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {

    Student findStudentById(Integer id);

    Integer deleteStudentById(Integer id);


}
