package com.school.app.studentservice.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author Eyüp Akay
 */
@Entity
@Table(name = "student")
@ToString
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Student implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false, unique = true)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "create_date", nullable = false, updatable = false)
    private LocalDate createDate;

    @PrePersist
    void onPrePersist() {
        this.createDate = LocalDate.now();
    }

}
