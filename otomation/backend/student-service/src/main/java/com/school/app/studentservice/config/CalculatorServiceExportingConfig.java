
package com.school.app.studentservice.config;

import com.school.app.studentservice.service.impl.ServiceServiceImpl;
import com.school.contract.studentservicelib.service.StudentService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.httpinvoker.HttpInvokerServiceExporter;

@Configuration
public class CalculatorServiceExportingConfig {

	@Bean(name = "studentService")
	StudentService instantiate() {
		return new ServiceServiceImpl();
	}

	@Bean(name = "/student")
	HttpInvokerServiceExporter serviceExporter(@Qualifier("studentService") StudentService service) {
		return getExporter(service, StudentService.class);
	}

	public static HttpInvokerServiceExporter getExporter(Object service, Class<?> serviceInterface) {
		HttpInvokerServiceExporter exporter = new HttpInvokerServiceExporter();
		exporter.setService(service);
		exporter.setServiceInterface(serviceInterface);
		return exporter;
	}
}
