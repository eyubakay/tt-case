package com.school.app.studentservice.service;

import com.school.app.studentservice.entity.Student;

import java.util.List;

public interface StudentRepositoryService {

    Student add(Student student);

    boolean delete(Integer id);

    Student update(Student student);

    Student get(Integer id);

    List<Student> list();
}
