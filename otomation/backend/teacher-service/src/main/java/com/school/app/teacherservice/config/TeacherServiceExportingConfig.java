
package com.school.app.teacherservice.config;

import com.school.app.teacherservice.service.impl.TeacherServiceImpl;
import com.school.contract.teacherservicelib.service.TeacherService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.httpinvoker.HttpInvokerServiceExporter;

@Configuration
public class TeacherServiceExportingConfig {

	@Bean(name = "teacherService")
	TeacherService instantiate() {
		return new TeacherServiceImpl();
	}

	@Bean(name = "/teacher")
	HttpInvokerServiceExporter serviceExporter(@Qualifier("teacherService") TeacherService service) {
		return getExporter(service, TeacherService.class);
	}

	public static HttpInvokerServiceExporter getExporter(Object service, Class<?> serviceInterface) {
		HttpInvokerServiceExporter exporter = new HttpInvokerServiceExporter();
		exporter.setService(service);
		exporter.setServiceInterface(serviceInterface);
		return exporter;
	}
}
