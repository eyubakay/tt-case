package com.school.app.teacherservice.service.impl;

import com.google.gson.Gson;
import com.school.app.teacherservice.dto.TeacherDtoMapper;
import com.school.app.teacherservice.entity.Teacher;
import com.school.app.teacherservice.service.TeacherRepositoryService;
import com.school.common.lib.aop.annotation.Loggable;
import com.school.contract.teacherservicelib.model.*;
import com.school.contract.teacherservicelib.service.TeacherService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author Eyüp Akay
 */
@Loggable
@Service
public class TeacherServiceImpl implements TeacherService {

    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Autowired
    private Gson gson;

    @Autowired
    private TeacherRepositoryService repositoryService;


    public void setGson(Gson gson) {
        this.gson = gson;
    }

    public void setRepositoryService(TeacherRepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }

    @Override
    public TeacherAddResponse add(TeacherAddRequest teacherAddRequest) {
        Teacher teacher = repositoryService.add(
                TeacherDtoMapper.getTeacherEntityFromTeacherDto(teacherAddRequest.getTeacher()));

        TeacherAddResponse response = TeacherAddResponse.builder().build();

        if (Objects.isNull(teacher) || Objects.isNull(teacher.getId())) {
            response.setStatus(false);
            response.setMessage("Success!");
        }

        TeacherDto studentDto = TeacherDtoMapper.getTeacherDtoFromTeacherEntity(teacher);

        response.setTeacher(studentDto);
        response.setStatus(true);
        response.setMessage("Successfully Saved!");

        return response;
    }

    @Override
    public TeacherUpdateResponse update(TeacherUpdateRequest teacherUpdateRequest) {
        return null;
    }

    @Override
    public TeacherGetResponse get(TeacherGetRequest teacherGetRequest) {
        Teacher teacher = repositoryService.get(teacherGetRequest.getTeacher().getId());

        TeacherGetResponse response = TeacherGetResponse.builder().build();

        if (Objects.isNull(teacher) || Objects.isNull(teacher.getId())) {
            response.setStatus(false);
            response.setMessage("Success!");
        }

        response.setTeacher(TeacherDtoMapper.getTeacherDtoFromTeacherEntity(repositoryService.get(teacherGetRequest.getTeacher().getId())));
        response.setStatus(true);
        response.setMessage("Success!");

        return response;
    }

    @Override
    public TeacherListResponse list(TeacherListRequest teacherListRequest) {
        return null;
    }

    @Override
    public TeacherDeleteResponse delete(TeacherDeleteRequest teacherDeleteRequest) {
        return null;
    }
}
