package com.school.app.teacherservice.repository;

import com.school.app.teacherservice.entity.Teacher;
import com.school.common.lib.aop.annotation.Loggable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * @author Eyüp Akay
 */
@Loggable
@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Integer> {

    Teacher findTeacherById(Integer id);
}
