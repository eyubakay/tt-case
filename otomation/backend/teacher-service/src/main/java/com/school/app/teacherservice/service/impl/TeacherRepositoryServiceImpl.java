package com.school.app.teacherservice.service.impl;

import com.school.app.teacherservice.entity.Teacher;
import com.school.app.teacherservice.repository.TeacherRepository;
import com.school.app.teacherservice.service.TeacherRepositoryService;
import com.school.common.lib.aop.annotation.Loggable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Loggable
public class TeacherRepositoryServiceImpl implements TeacherRepositoryService {

    @Autowired
    private TeacherRepository repository;

    @Override
    public Teacher add(Teacher teacher) {
        return repository.save(teacher);
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public Teacher update(Teacher teacher) {
        return null;
    }

    @Override
    public Teacher get(Integer id) {
        return repository.findTeacherById(id);
    }

    @Override
    public List<Teacher> list() {
        return null;
    }
}
