package com.school.app.teacherservice.service;

import com.school.app.teacherservice.entity.Teacher;

import java.util.List;

public interface TeacherRepositoryService {

    Teacher add(Teacher teacher);

    boolean delete(Integer id);

    Teacher update(Teacher teacher);

    Teacher get(Integer id);

    List<Teacher> list();
}
