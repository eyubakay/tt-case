package com.school.app.teacherservice.dto;

import com.school.app.teacherservice.entity.Teacher;
import com.school.contract.teacherservicelib.model.TeacherDto;

public class TeacherDtoMapper {

    public static Teacher getTeacherEntityFromTeacherDto(TeacherDto student) {
        return Teacher.builder()
                .id(student.getId())
                .name(student.getName())
                .surname(student.getSurname())
                .build();
    }


    public static TeacherDto getTeacherDtoFromTeacherEntity(Teacher teacher) {
        return TeacherDto.builder()
                .name(teacher.getName())
                .surname(teacher.getSurname())
                .id(teacher.getId())
                .build();
    }


}
