package com.school.app.teacherservice.service;

import com.school.app.teacherservice.dto.TeacherDtoMapper;
import com.school.app.teacherservice.entity.Teacher;
import com.school.app.teacherservice.repository.TeacherRepository;
import com.school.contract.teacherservicelib.model.TeacherAddRequest;
import com.school.contract.teacherservicelib.model.TeacherDto;
import com.school.contract.teacherservicelib.model.TeacherGetRequest;
import com.school.contract.teacherservicelib.model.TeacherGetResponse;
import com.school.contract.teacherservicelib.service.TeacherService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class TeacherServiceTests {


    @Autowired
    TeacherService teacherService;

    @Autowired
    TeacherRepositoryService repositoryService;

    @Autowired
    TeacherRepository repository;

    @BeforeEach
    void setUp() {
        repository.deleteAll();
    }

    @Test
    @Order(2)
    void test_repository_add_success() {
        Teacher expectedTeacher = getTeacher();

        repository.save(expectedTeacher);

        Teacher actualTeacher = repository.findTeacherById(1);

        Assertions.assertNotNull(actualTeacher);
        Assertions.assertNotNull(actualTeacher.getId());
        Assertions.assertEquals(expectedTeacher.getName(), actualTeacher.getName());
    }

    @Test
    @Order(1)
    void test_add_success() {
        TeacherAddRequest expectedStudent = TeacherAddRequest.builder()
                .teacher(TeacherDtoMapper.getTeacherDtoFromTeacherEntity(getTeacher()))
                .build();

        teacherService.add(expectedStudent);

        TeacherGetResponse response = teacherService.get(TeacherGetRequest.builder()
                .teacher(TeacherDto.builder().id(Integer.valueOf(1)).build()).build());

        Assertions.assertNotNull(response);
        Assertions.assertTrue(response.getStatus());
        Assertions.assertNotNull(response.getTeacher().getName());
        Assertions.assertEquals(expectedStudent.getTeacher().getName(), response.getTeacher().getName());
    }

    private Teacher getTeacher() {
        return Teacher.builder()
                .name("Eyüp")
                .surname("Akay")
                .build();
    }


}
