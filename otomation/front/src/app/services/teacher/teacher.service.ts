
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
// import {TeacherInfo} from '../modal/simple-machine-info';
// import {catchError, map, tap} from "rxjs/operators";
// import {delay} from 'rxjs/operators';
import 'rxjs/add/operator/map';
import {TeacherInfo} from '../../components/modal/teacher-info';

@Injectable()
export class TeacherService { // extends CoreHttpService {

  private readonly baseUrl: string;
  headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
  private readonly teacherEndpoint: string;

  constructor(private http: HttpClient) {
    this.teacherEndpoint = 'teacher';
  }


  public fetchAll(): Observable<TeacherInfo[]> {
    return this.http.get<TeacherInfo[]>(this.baseUrl + this.teacherEndpoint, {headers: this.headers});
  }
  //
  // public save(machine: MachineCreateRequest) {
  //   const body = JSON.stringify(machine);
  //
  //   return this.http.post<SimpleMachineInfo>(this.baseUrl + this.machineEndpoint, body,
  //       {headers: this.headers}).subscribe({
  //     next: data => {
  //       console.log(data);
  //     },
  //     error: error => {
  //       console.log(error.message);
  //       console.error('There was an error!', error);
  //     }
  //   });
  // }
  //
  // public fetchMachineInfo(machineId: string): Observable<SimpleMachineInfo> {
  //   return this.http.get<SimpleMachineInfo>(
  //       this.baseUrl + this.machineEndpoint + '/' + machineId, {headers: this.headers})
  //       .pipe(
  //           tap(_ => this.log(`fetched machine id=${machineId}`)),
  //           catchError(this.handleError<SimpleMachineInfo>(`getMachine id=${machineId}`))
  //       );
  // }
  //
  // public persist1(machine: MachineCreateRequest): Observable<SimpleMachineInfo> {
  //   const body = JSON.stringify(machine);
  //   this.delay(3000);
  //   return this.http.post<SimpleMachineInfo>(this.baseUrl + this.machineEndpoint, body, {headers: this.headers})
  //       .pipe(
  //           tap((newMachine: SimpleMachineInfo) => this.log(`added machine w/ id=${newMachine.id}`)),
  //           map((data) => {
  //             return data;
  //           }),
  //           catchError(this.handleError<SimpleMachineInfo>('addMachine'))
  //       );
  // }
  //
  // public persist(machine: MachineCreateRequest): Observable<SimpleMachineInfo> {
  //   const body = JSON.stringify(machine);
  //   return this.http.post<SimpleMachineInfo>(this.baseUrl + this.machineEndpoint, body,
  //       {headers: this.headers}).pipe(
  //       tap((newMachine: SimpleMachineInfo) => this.log(`added machine w/ id=${newMachine.id}`)),
  //       catchError(this.handleError<SimpleMachineInfo>('addMachine'))
  //   );
  // }

}

