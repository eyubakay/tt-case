 import { Injectable } from '@angular/core';
 import {TeacherInfo} from '../../components/modal/teacher-info';
 import {HttpClient, HttpHeaders} from '@angular/common/http';
 import {Observable} from 'rxjs/Observable';
 import {StudentInfo} from '../../components/modal/student-info';
 import {environment} from '../../../environments/environment';
 import {map} from 'rxjs/operator/map';
 import {StudentRequest} from '../../components/modal/student-request';

 @Injectable()
 export class StudentService {

   private baseUrl: string;
   headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
   private studentEndpoint: string;

   constructor(private http: HttpClient) {
     this.studentEndpoint = 'student';
   }

   public fetchAll(): Observable<StudentInfo[]> {
     const body = JSON.stringify('{}');
     this.baseUrl = environment.backendBaseUrl;
     return this.http.get<StudentInfo[]>(this.baseUrl + this.studentEndpoint, {headers: this.headers});
   }

   public fetchStudentInfo(id: number): Observable<StudentInfo> {
     return this.http.get<StudentInfo>(
         this.baseUrl + this.studentEndpoint + '/' + id, {headers: this.headers});
   }

   public persist(request: StudentInfo): Observable<StudentInfo> {
     const req: StudentRequest = {
       student: request
     };
     const body = JSON.stringify(req);
     console.info("persist Student request:" + body);

     return this.http.post<StudentInfo>(
         this.baseUrl + this.studentEndpoint, body, {headers: this.headers})
         .map((data) => {
           console.log(data)
           // if(data) {
           //   if (data.code == 200) {
           //     this.toastr.success(data.message,"Success");
           //     this.router.navigate(['/']);
           //     return data;
           //
           //   }else{
           //     this.toastr.error(data.message,"Fail");
           //   }
           // }
           return data;
         });
   }


   // Get all students list via API or any data storage
   getAllStudents(){
     let studentList:any;
     if(localStorage.getItem('students') && localStorage.getItem('students') != '') {
       studentList = {
         code : 200,
         message : "Students List Fetched Successfully",
         data : JSON.parse(localStorage.getItem('students'))
       }
     }else{
       studentList = {
         code : 200,
         message : "Students List Fetched Successfully",
         data : JSON.parse(localStorage.getItem('students'))
       }
     }
     return studentList;
   }

   doRegisterStudent(data, index){


     const student: StudentInfo = {
       id: '0',
       name: data['first_name'],
       surname: data['first_name']
     };

     console.log("data" + JSON.stringify(data))
     console.log("student" + JSON.stringify(student))
     console.log("index" + index)
     return this.persist(student);
     // persist(data),
     // let studentList = JSON.parse(localStorage.getItem('students'));
     // let returnData;
     // console.log("index", index);
     // if(index != null) {
     //
     //
     //   for (var i = 0; i < studentList.length; i++) {
     //     if (index != i && studentList[i].email == data.email) {
     //       returnData = {
     //         code : 503,
     //         message : "Email Address Already In Use",
     //         data : null
     //       }
     //       return returnData;
     //     }
     //   }
     //
     //   studentList[index] = data;
     //   localStorage.setItem('students',JSON.stringify(studentList));
     //   returnData = {
     //     code : 200,
     //     message : "Student Successfully Updated",
     //     data : JSON.parse(localStorage.getItem('students'))
     //   }
     // }else{
     //   data.id = this.generateRandomID();
     //   for (var i = 0; i < studentList.length; i++) {
     //     if (studentList[i].email == data.email) {
     //       returnData = {
     //         code : 503,
     //         message : "Email Address Already In Use",
     //         data : null
     //       }
     //       return returnData;
     //     }
     //   }
     //   studentList.unshift(data);
     //
     //   localStorage.setItem('students',JSON.stringify(studentList));
     //
     //   returnData = {
     //     code : 200,
     //     message : "Student Successfully Added",
     //     data : JSON.parse(localStorage.getItem('students'))
     //   }
     // }
     // return returnData;
   }

   deleteStudent(index: number) {

     const student: StudentInfo = {
       id: index.toString(),
       name: '',
       surname: ''
     }


     const httpOptions = {
       headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body: JSON.stringify(student)
     };

     return new Promise(resolve => {
       this.http.delete(this.baseUrl + this.studentEndpoint, httpOptions)
           .subscribe(res => {
             resolve(res);
             return true;
           }, err => {
             resolve(err);
             return false;
           });
     });
   }





   getStudentDetails(index: number){
     // let studentList = JSON.parse(localStorage.getItem('students'));
     //
     // let returnData = {
     //   code : 200,
     //   message : "Student Details Fetched",
     //   studentData : studentList[index]
     // }

     return this.fetchStudentInfo(index);
   }


   generateRandomID() {
     var x = Math.floor((Math.random() * Math.random() * 9999));
     return x;
   }

 }
