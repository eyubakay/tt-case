import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';

// Services
import {routerTransition} from '../../../services/config/config.service';
import {TeacherService} from '../../../services/teacher/teacher.service';

@Component({
 	selector: 'app-teacher-list',
 	templateUrl: './teacher-list.component.html',
 	styleUrls: ['./teacher-list.component.css'],
 	animations: [routerTransition()],
 	host: {'[@routerTransition]': ''}
 })

 export class TeacherListComponent implements OnInit {
 	teacherList: any;
 	teacherListData: any;
 	constructor(private teacherService: TeacherService, private toastr: ToastrService) { }
 	// Call student list function on page load 
 	ngOnInit() {
		// this.getTeachersList();
 	}

 	// Get student list from services
 	getTeachersList() {
		this.teacherList = this.teacherService.fetchAll();
 		// this.success(teacherList);
 	}

 	// Get student list success
 	success(data){
 		this.teacherListData = data.data;
 		for (var i = 0; i < this.teacherListData.length; i++) {
 			this.teacherListData[i].name = this.teacherListData[i].first_name +' '+ this.teacherListData[i].last_name;
 		}
 	}
	 //
 	// // Delete a student with its index
 	// deleteStudent(index:number){
 	// 	// get confirm box for confirmation
 	// 	let r = confirm("Are you sure?");
 	// 	if (r == true) {
 	// 		let studentDelete = this.teacherService.deleteStudent(index);
 	// 		if(studentDelete) {
 	// 			this.toastr.success("Success", "Student Deleted");
 	// 		}
 	// 		this.getTeachersList();
 	// 	}
 	// }
 }
