
export class TeacherInfo {
    id: string;
    name: string;
    surname: string;
    branch: string;
    mail: string;
    phone: string;
}
