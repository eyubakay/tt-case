import {StudentInfo} from './student-info';

export interface StudentRequest {
    student: StudentInfo;
}
