
export interface StudentInfo {
    id: string;
    name: string;
    surname: string;
}
