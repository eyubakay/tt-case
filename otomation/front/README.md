# Course Management



## Feature list

 * Admin Login & Logout
 * Student Listing
 * Student Searching
 * Student Details
 * Student Addition
 * Student Update
 * Student Deletion


Burada yapacağınız projeyi bitirmeniz beklenmemektedir. Sadece kod kültürünüz incelenmesi hedeflenmektedir. Java da yazmanız beklenmektedir.
Reactjs kullanımı, Spring boot, r2jdbc veya jpa ve mickroservis mimarisi kullanmanız ek değerlendirecektir.

Türkçe ve Matematik Kurs Otomasyonu
1 kursun otomasyonu planlanmaktadır.
Kursta branş öğretmenleri, öğrenciler ve Türkçe ve matematik Dersleri vardır. (bu kurs sadece ve Türkçe ve matematik kursudur.)
Öğretmenlerin id, adı soyadı, verebildiği ders tutulmalıdır.
Türkçe öğretmeni matematik dersi, matematik öğretmeni ise Türkçe dersi verememektedir.
Öğrencinin numarası, adı, soyadı adresi, hangi öğretmenden haftalık kaç saat aldığı tutulmaktadır.
Kurs hafta sonu saat 8 ile 16 saatleri arasında verilecektir. (Cumartesi, pazar öğle yemeği tatili 12 ile 13 arası olacaktır.)
Her ders 50 dakika ve 10 dakika ara vardır.
•	Öğretmen listeleme, giriş ve güncelleme yapılabilmelidir.
•	Öğrenci listeleme, giriş ve güncelleme yapılabilmelidir.
•	Hangi öğrenci hangi öğretmenden hangi dersi saat kaçta alacağı belirtilmelidir.
•	Hangi Öğrenci hangi dersi hangi öğretmenden ne zaman alıyor bu bilgileri gösterilebilmelidir.
